package helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import com.sun.org.apache.bcel.internal.generic.Type;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class SBB_data {
	
	public static class SBB_time{
		SBB_time(String sbbString){
			int firstPart = Integer.parseInt(sbbString.substring(0, 3));
			int minutes =Integer.parseInt(sbbString.substring(3, 5));
			if(firstPart<5)firstPart+=24;
			minutesFromMidnight=firstPart*60+minutes;
		}
		int getMinutes(){
			return minutesFromMidnight%60;
		}
		int getHours(){
			return (minutesFromMidnight-getMinutes())/60;
		}
		
		@Override
		public String toString(){
			String minuteString = ""+getMinutes();
			if(minuteString.length()==1)minuteString="0"+minuteString;
			return getHours()+":"+minuteString;
		}
		
		public boolean before(SBB_time other){
			return minutesFromMidnight < other.minutesFromMidnight;
		}
		
		public boolean after(SBB_time other){
			return minutesFromMidnight > other.minutesFromMidnight;
		}
		
		public int minutesFromMidnight;
	}

	public void addServices(BufferedReader reader){
		String nextLine = null;
		Boolean readingStars = true;
		String currentServiceString = "";
		while(true){
			try {
				nextLine = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(nextLine == null)break;
			
			if(readingStars&&nextLine.charAt(0)!='*'){
				readingStars = false;
			}
			
			if(!readingStars&&nextLine.charAt(0)=='*'){
				Train_service ts = new Train_service(currentServiceString);
				//only for now...
				//if we have faster algorithms use also smaller trains
				if(ts.trainType.equals("ICE")
						||ts.trainType.equals("IC ")
						||ts.trainType.equals("IR ")
						||ts.trainType.equals("EC ")
						||ts.trainType.equals("ICN")
						||true)
					services.add(ts);//only take fast trains
				currentServiceString = "";
				readingStars = true;
			}
			
			currentServiceString+=nextLine+"\n";
			
			
		}
	}
	
	public void addStations(BufferedReader reader){
		String nextLine = null;
		while(true){
			try {
				nextLine = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(nextLine == null)break;
			Train_station newStation = new Train_station(nextLine);
			stationHash.put(newStation.ID, newStation);
			stations.add(newStation);
		}
	}
	
	public void connect(){
		for(Train_service service: services)service.connect();
	}
	
	
	HashMap<Integer, Train_station> stationHash = new HashMap<>();
	List<Train_station> stations = new LinkedList<Train_station>();
	List<Train_service> services = new LinkedList<Train_service>();
	
	
	public class Train_station{
		Train_station(String sbbString){
			ID = Integer.parseInt(sbbString.substring(0, 7));
			stationName = sbbString.substring(39);
			xCoord = Float.parseFloat(sbbString.substring(8, 18));
			yCoord = Float.parseFloat(sbbString.substring(19, 29));
		}
		
		int timeDistanceTo(Train_station other){
			float xdist = (xCoord-other.xCoord);
			float ydist = (yCoord-other.yCoord);
			return (int)(Math.sqrt(xdist*xdist+ydist*ydist)/3);//just a wild guess something over 200 km/h
		}
		
		@Override
		public boolean equals(Object other){
			if(other.getClass().equals(this.getClass()))
				return ID==((Train_station)other).ID;
			return false;
		}
		
		public List<Train_service.Stop> getConnectionsInTime(SBB_time dFrom, SBB_time dTo){
			LinkedList<Train_service.Stop>results = new LinkedList<>();
			for(Train_service.Stop stop: trainStops){
				//search for correct stop

				if(stop.departure!=null&&stop.departure.after(dFrom)&&stop.departure.before(dTo)){
					if(stop.getNextStop().arrival.before(dTo))results.add(stop);
				}
			}
			return results;
		}
		
		public int ID;
		public String stationName;
		public float xCoord;
		public float yCoord;
		//TODO: getTrainServices from time to time
		//public List<Train_service> trainServices = new LinkedList<>();
		public List<Train_service.Stop> trainStops = new LinkedList<>();
	}
	
	
	
	public class Train_service{
		Train_service(String sbbServiceString){
			String lines[] = sbbServiceString.split("\\r?\\n");
		    for(String iString: lines)
		    {
		    	if(iString.charAt(0)=='*'){
		    		//metadata
		    		if(iString.charAt(1)=='G'){
		    			trainType = iString.substring(3,6);
		    		}
		    	}
		    	else stops.add(new Stop(iString, this));
		    }
		}
		
		@Override
		public String toString(){
			String result = trainType+" from "+stops.getFirst().stationName+" to "+stops.getLast().stationName+"\ndeparting "+stops.getFirst().departure.toString()+"\n";
			for(Stop stop: stops){
				//result+=stop.station.stationName+"\n";
			}
			return result;
		}
		
		
		public class Stop{
			Stop(String sbbStopString, Train_service parentService){
				stationNumber = Integer.parseInt(sbbStopString.substring(0, 7));
				stationName = sbbStopString.substring(8, 29);
				service = parentService;
			    //SimpleDateFormat ft = new SimpleDateFormat ("HHHmm");
				
				//try {
					//TODO: test if "-"
					isStation = sbbStopString.charAt(29)!='-';
					String substring = sbbStopString.substring(30, 35);
					if(substring.charAt(0)!=' '){
						arrival = new SBB_time(substring);
					}
					substring = sbbStopString.substring(37, 42);
					if(substring.charAt(0)!=' '){
						departure = new SBB_time(substring);
					}
				//} catch (ParseException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				//}
				
			}
			public void connect(){
				station = stationHash.get(stationNumber);
				if(station!=null)station.trainStops.add(this);
			}
			public Stop getNextStop(){
				//remark: this is very slow and should be used seldom
				boolean foundOwn = false;
				for(Stop stop: service.stops){
					if(foundOwn)return stop;
					if(stop.station.equals(station))foundOwn = true;
				}
				return null;
			}
			public int stationNumber;
			public boolean isStation;
			public Train_service service;
			public String stationName;
			public SBB_time arrival;
			public SBB_time departure;
			public Train_station station;	
		}
		public void connect(){
			for(Stop stop: stops) stop.connect();
		}
		public LinkedList<Stop>stops = new LinkedList<Stop>();
		public String trainType;
	}
	
	public void printServices(String stationn){
		for(Train_station station: stations){
			//if(station.stationName.toLowerCase().contains(stationn.toLowerCase()))
			if(station.stationName.toLowerCase().contains(stationn.toLowerCase()))
			{
				System.out.println("found it: ");
				System.out.println(station.stationName);
				for(SBB_data.Train_service.Stop stop: station.trainStops){
					System.out.println(stop.service.toString());
				}
			}
		}
	}
	
	public Train_station getFirstOccurence(int tsid){
		for(Train_station station: stations){
			if(tsid == station.ID)
			{
				//System.out.println("found it: ");
				//System.out.println(station.stationName);
				return station;
			}
		}
		return null;
	}
	
	public Train_station getFirstOccurence(String stationn){
		for(Train_station station: stations){
			//if(station.stationName.toLowerCase().contains(stationn.toLowerCase()))
			if(station.stationName.toLowerCase().equals(stationn.toLowerCase()))
			{
				//System.out.println("found it: ");
				//System.out.println(station.stationName);
				return station;
			}
		}
		return null;
	}
	
	public Train_station smartSearch(String stationn){
		
		stationn = stationn.toLowerCase();
		int shortestDistance = 1000000;
		Train_station bestStation = null;
		
		for(Train_station station: stations){
			int dist = SBB_Magic.LevenshteinDistance(stationn, station.stationName.toLowerCase());	
			if(dist<shortestDistance){
				shortestDistance = dist;
				bestStation = station;
			}
		}
		
		return bestStation;
	}
}
