package helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.RandomAccess;
import java.util.Stack;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class SBB_Magic {
	static class Journey{
		class Leg{
			SBB_data.Train_service.Stop depStop;
			SBB_data.Train_service.Stop arrStop;	
			
			public long getTravelTime(){
				//returns time in minute
				return arrStop.arrival.minutesFromMidnight
						-depStop.departure.minutesFromMidnight;
			}
		}
		
		public LinkedList<Leg> legs = new LinkedList<Leg>();
		

		public long getTravelTime(){
			return legs.getLast().arrStop.arrival.minutesFromMidnight
					-legs.getFirst().depStop.departure.minutesFromMidnight;
		}
		
		public void addLeg(Leg newLeg){
			if(!legs.getLast().arrStop.station.equals(newLeg.depStop.station))
				throw new NotImplementedException();
			else
				legs.add(newLeg);
		}
	}

	static String getJSON(List<recursiveJourney> journeys){
		String result = "{\"connections\":[";
		for(recursiveJourney journey: journeys){
			result+=journey.BuildJourneyString()+",";
		}
		result= result.substring(0, result.length()-1);
		result+="]}";
		return result;
	}
	
	static class recursiveJourney{
		public recursiveJourney(Leg newLeg, recursiveJourney oldJourney){
			lastJourney = oldJourney;
			thisLeg = newLeg;
		}

		
		public static class Leg{
			public Leg(SBB_data.Train_service.Stop dep, SBB_data.Train_service.Stop arr){
				depStop = dep;
				arrStop = arr;
			}
			SBB_data.Train_service.Stop depStop;
			SBB_data.Train_service.Stop arrStop;	
			public long getTravelTime(){
				//returns time in minute
				return arrStop.arrival.minutesFromMidnight
						-depStop.departure.minutesFromMidnight;
			}
			
			public void printLeg(){
				System.out.println(depStop.service.trainType+": "
						+depStop.departure+ ": "+depStop.stationName
						+" -> "+arrStop.arrival+": "+arrStop.stationName);
			}
		}
		
		@Override
		public boolean equals(Object other){
			if(!other.getClass().equals(getClass()))return false;
			else{
				recursiveJourney otherJourney = (recursiveJourney)other;
				if(thisLeg.arrStop.station.equals(otherJourney.thisLeg.arrStop.station)
						&& thisLeg.depStop.station.equals(otherJourney.thisLeg.depStop.station)){
					if(lastJourney!=null&&otherJourney.lastJourney!=null)return lastJourney.equals(otherJourney.lastJourney);
					else if(lastJourney==null&&otherJourney.lastJourney==null)return true;
				}
			}
			return false;
		}

		public String BuildJourneyString(){
			return "{\"sections\":["+jsonTraverse()+"]}";
		}
		
		public String jsonTraverse(){
			if(lastJourney!=null)return lastJourney.jsonTraverse()+","+sectionJson();
			else return sectionJson();
		}
		
		public String sectionJson(){
			getNearBeatyObjects();
			String result = "{\"type:\":\""+thisLeg.arrStop.service.trainType+"\","
					+"\"departure\":\""+thisLeg.depStop.departure+"\","
					+"\"arrival\":\""+thisLeg.arrStop.arrival+"\","
					+"\"departurestation\":\""+thisLeg.depStop.station.stationName+"\","
					+"\"arrivalstation\":\""+thisLeg.arrStop.station.stationName+"\","
					+"\"attractions\":[";		
			boolean first =true;
			for(beautyOfSwitzerland.beautyObject object: beautyObjectsNear){
				if(first)first = false;
				else result+=",";
				result+="{\"ID\":\""+object.ID+"\",\"name\":\""+object.name+"\",\"type\":\""+object.type+"\"}";
			}

				result+="]}";
			return result;
		}
		
		public Leg getFirstLeg(){
			if(lastJourney==null)return thisLeg;
			else return lastJourney.getFirstLeg();
		}
		
		public int getTravelTime(){
			return thisLeg.arrStop.arrival.minutesFromMidnight
					-getFirstLeg().depStop.departure.minutesFromMidnight;
		}
		
		public void printJourney(){
			if(lastJourney!=null)lastJourney.printJourney();
			thisLeg.printLeg();
		}
		
		Leg thisLeg;
		
		List<beautyOfSwitzerland.beautyObject> beautyObjectsNear = new LinkedList<>();
		public void getNearBeatyObjects(){
			for(beautyOfSwitzerland.beautyObject entry: nearestLeg.keySet()){
				if(nearestLeg.get(entry)==thisLeg){
					//test if nearer than
					beautyObjectsNear.add(entry);
				}
			}
		}
		
		recursiveJourney lastJourney = null;
		
		float beautyValue;
		
		HashMap<beautyOfSwitzerland.beautyObject, Leg> nearestLeg;
		
		public float RateBeauty(HashMap<Integer, Boolean> interest, beautyOfSwitzerland beauty){
			HashMap<Integer, Float> distance = new HashMap<>();
			nearestLeg = new HashMap<>();
			computeDistBeau(distance, nearestLeg, beauty);
			float resultValue = 0;
			for(beautyOfSwitzerland.beautyObject object: beauty.beList){
				if(interest.containsKey(object.ID)&&interest.get(object.ID))resultValue += 1/distance.get(object.ID);
				// resultValue += 1/distance.get(object.ID);
				//if(interest.get(object.ID)&&distance.get(object.ID)<30);
			}			
			beautyValue = resultValue;
			return resultValue;
		}
		
		public void computeDistBeau(HashMap<Integer, Float> distance, HashMap<beautyOfSwitzerland.beautyObject, Leg> nearestLega, beautyOfSwitzerland beauty){
			nearestLeg = nearestLega;
			if(lastJourney!=null)lastJourney.computeDistBeau(distance, nearestLega, beauty);
			for(beautyOfSwitzerland.beautyObject object: beauty.beList){				
				SBB_data.Train_service.Stop currentStop = thisLeg.depStop;
				while(!currentStop.station.equals(thisLeg.arrStop.station)){
					float xD = currentStop.station.xCoord-object.xCoord;
					float yD = currentStop.station.yCoord-object.yCoord;
					float thisDistance = (float)Math.sqrt(yD*yD+xD*xD);
					if(distance.containsKey(object.ID)){
						if(distance.get(object.ID)>thisDistance){
							distance.put(object.ID, thisDistance);
							
							
							if(thisDistance<15){
								nearestLeg.remove(object);
								nearestLeg.put(object, thisLeg);
							}
						}
					}
					else distance.put(object.ID, thisDistance);
					currentStop = currentStop.getNextStop();
				}
			}
		}
		
		float diversityRating;
		boolean diversityRatingComputed = false;
		
		float computeDiversityRating(){
			if(diversityRatingComputed)return diversityRating;
			else{
				if(lastJourney!=null)
				{
					diversityRating = lastJourney.computeDiversityRating()+computeDiversityStep(thisLeg.arrStop.station.xCoord, thisLeg.arrStop.station.yCoord);
				}
				else{
					diversityRating = computeDiversityStep(thisLeg.arrStop.station.xCoord, thisLeg.arrStop.station.yCoord);
				}
				return diversityRating;
			}
		}
		
		float computeDiversityStep(float X, float Y){
			float dX = X-thisLeg.arrStop.station.xCoord;
			float dY = Y-thisLeg.arrStop.station.yCoord;
			dX*=dX;
			dY*=dY;
			
			float samePenalty = 0;
			if(X==thisLeg.arrStop.station.xCoord&&Y==thisLeg.arrStop.station.yCoord)samePenalty = 10;
			/*
			if(lastJourney!=null)//Play with this
				return (float)Math.sqrt(Math.sqrt(dX+dY))+lastJourney.computeDiversityStep(X,Y);
			else
				return (float)Math.sqrt(Math.sqrt(dX+dY));
				*/
			if(lastJourney!=null)//Play with this
				return dX+dY+lastJourney.computeDiversityStep(X,Y)-samePenalty;
			else
				return dX+dY-samePenalty;
		}
		
		public List<SBB_data.Train_station> getStations(){
			if(lastJourney!=null){
				 List<SBB_data.Train_station> stations = lastJourney.getStations();
				 stations.add(thisLeg.arrStop.station);
				 return stations;
			}
			else{
				 List<SBB_data.Train_station> stations = new LinkedList<>();
				 stations.add(thisLeg.arrStop.station);
				 return stations;
			}
		}
		
		float originality = 0;
		
		public float compareSimilarity(SBB_Magic.recursiveJourney journey){
			List<SBB_data.Train_station> ownStations = getStations();
			List<SBB_data.Train_station> otherStations = journey.getStations();
			float sum = 0;
			for(SBB_data.Train_station ownStation: ownStations){
				for(SBB_data.Train_station otherStation: otherStations){
					float dX = ownStation.xCoord-otherStation.xCoord;
					float dY = ownStation.yCoord-otherStation.yCoord;
					dX*=dX;
					dY*=dY;
					sum+=Math.sqrt(Math.sqrt(dX+dY));
				}
			}
			originality += sum;
			return sum;
		}
		
	}
	
	
	static List<recursiveJourney> getAllJourneys(SBB_data.SBB_time fromTime, SBB_data.Train_station fromStation, SBB_data.SBB_time toTime,SBB_data.Train_station toStation){
		//Stack<recursiveJourney> lifo = new Stack<SBB_Magic.recursiveJourney>();
		Queue<recursiveJourney> fifo = new LinkedList<SBB_Magic.recursiveJourney>();
		LinkedList<recursiveJourney> result = new LinkedList<recursiveJourney>();
		
		List<SBB_data.Train_service.Stop> connects = fromStation.getConnectionsInTime(fromTime, toTime);
		for(SBB_data.Train_service.Stop connect: connects){
			//test for all possible legs
			SBB_data.Train_service.Stop testStop;
			testStop = connect;
			while(testStop.getNextStop()!=null){
				//test if this stop is possible
				testStop = testStop.getNextStop();
				if(testStop.isStation&&testStop.arrival.minutesFromMidnight+testStop.station.timeDistanceTo(toStation)<toTime.minutesFromMidnight){
					//can be possible...
					fifo.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), null));
					if(fifo.size()>1000)
						decreaseLoad(fifo, 500);
				}

			}
		}
		int counter = 0;
		while(!fifo.isEmpty()&&counter<50){
			counter++;
			recursiveJourney lastJourney = fifo.poll();			
			connects = lastJourney.thisLeg.arrStop.station.getConnectionsInTime(
					lastJourney.thisLeg.arrStop.arrival, toTime);
			for(SBB_data.Train_service.Stop connect: connects){
				//test for all possible legs
				SBB_data.Train_service.Stop testStop;
				testStop = connect;
				while(testStop.getNextStop()!=null){
					testStop = testStop.getNextStop();
					//test if this stop is possible
					int timeDistance = testStop.station.timeDistanceTo(toStation);
					if(testStop.isStation&&testStop.arrival.minutesFromMidnight+timeDistance<toTime.minutesFromMidnight){
						//can be possible...
						if(!testStop.station.equals(toStation))
						{
							fifo.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), lastJourney));
							if(fifo.size()>100)decreaseLoad(fifo, 50);
						}
						else
						{
							result.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), lastJourney));
							//if(result.size()>5)return result;
						}
					}
				}
			}
		}
		
		decreaseLoad(result, 3);
		List<recursiveJourney> journeys = new LinkedList<>();
		
		

		
		
		/*
		//slow part
		for(recursiveJourney journeyOne: journeys){
			for(recursiveJourney journeyTwo: journeys){
				journeyOne.compareSimilarity(journeyTwo);
			}
		}*/
		
		

		return result;
	}
	
	
	
	static List<recursiveJourney> getAllJourneys(SBB_data.SBB_time fromTime, SBB_data.Train_station fromStation, SBB_data.SBB_time toTime,SBB_data.Train_station toStation, beautyOfSwitzerland beautyOfSwiss, HashMap<Integer, Boolean> interest, List<recursiveJourney>mindList){
		//Stack<recursiveJourney> lifo = new Stack<SBB_Magic.recursiveJourney>();
		LinkedList<recursiveJourney> fifo = new LinkedList<SBB_Magic.recursiveJourney>();
		LinkedList<recursiveJourney> result = new LinkedList<recursiveJourney>();
		
		List<SBB_data.Train_service.Stop> connects = fromStation.getConnectionsInTime(fromTime, toTime);
		for(SBB_data.Train_service.Stop connect: connects){
			//test for all possible legs
			SBB_data.Train_service.Stop testStop;
			testStop = connect;
			while(testStop.getNextStop()!=null){
				//test if this stop is possible
				testStop = testStop.getNextStop();
				if(testStop.isStation&&testStop.arrival.minutesFromMidnight+testStop.station.timeDistanceTo(toStation)<toTime.minutesFromMidnight){
					//can be possible...
					fifo.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), null));
					if(fifo.size()>1000) decreaseLoadV2(fifo, 500, beautyOfSwiss, interest, mindList);
						//decreaseLoad(fifo, 500, beautyOfSwiss, interest);

				}

			}
		}
		int counter = 0;
		while(!fifo.isEmpty()&&counter<50){
			counter++;
			recursiveJourney lastJourney = fifo.poll();			
			connects = lastJourney.thisLeg.arrStop.station.getConnectionsInTime(
					lastJourney.thisLeg.arrStop.arrival, toTime);
			for(SBB_data.Train_service.Stop connect: connects){
				//test for all possible legs
				SBB_data.Train_service.Stop testStop;
				testStop = connect;
				while(testStop.getNextStop()!=null){
					testStop = testStop.getNextStop();
					//test if this stop is possible
					int timeDistance = testStop.station.timeDistanceTo(toStation);
					if(testStop.isStation&&testStop.arrival.minutesFromMidnight+timeDistance<toTime.minutesFromMidnight){
						//can be possible...
						if(!testStop.station.equals(toStation))
						{
							fifo.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), lastJourney));
							//if(fifo.size()>1000)decreaseLoad(fifo, 500, beautyOfSwiss, interest);
							if(fifo.size()>1000)decreaseLoadV2(fifo, 500, beautyOfSwiss, interest, mindList);
						}
						else
						{
							result.add(new recursiveJourney(new recursiveJourney.Leg(connect, testStop), lastJourney));
							//if(result.size()>5)return result;
						}
					}
				}
			}
		}
		
		//decreaseLoad(result, 3, beautyOfSwiss, interest);
		decreaseLoadV2(result, 3, beautyOfSwiss, interest, mindList);		

		return result;
	}

	
	
	
	static List<recursiveJourney> getDiverseRoute(SBB_data.SBB_time fromTime, SBB_data.Train_station fromStation, SBB_data.SBB_time toTime,SBB_data.Train_station toStation, beautyOfSwitzerland beautyOfSwiss, HashMap<Integer, Boolean> interest, int numberOfRoutes){
		List<recursiveJourney> routeList = new LinkedList<>();
		for(int i=0; i<numberOfRoutes; i++){
			List<recursiveJourney> results = getAllJourneys(fromTime, fromStation, toTime, toStation, beautyOfSwiss, interest, routeList);
			if(results.isEmpty())return routeList;
			else routeList.add(results.get(0));
		}
		return routeList;
	}
	
	
	static void decreaseLoad(Queue<recursiveJourney> queue, int numberOfElements){

		
		int counter = 0;
		
		Random rnd = new Random();
		float threshold = 1;
		while(queue.size()>numberOfElements){
			recursiveJourney candidate = queue.poll();
			float result = candidate.computeDiversityRating();
			threshold = threshold*0.9f+result*0.1f;
			if(candidate.computeDiversityRating()+100*rnd.nextFloat()-50>threshold){
				queue.add(candidate);
			}
			counter++;
			if(counter>1000){
				int a=1;
			}
			//System.out.println("removed elements in "+counter+"steps. Threshold: "+ threshold+" result: "+result);
		}
		//System.out.println("removed elements in "+counter+"steps");
		
			while(queue.size()>numberOfElements){
			queue.poll();}
	}

	static void decreaseLoad(LinkedList<recursiveJourney> queue, int numberOfElements, beautyOfSwitzerland beautyOfSwiss, HashMap<Integer, Boolean> interest){

		
		
		for(recursiveJourney journey:queue){
			journey.RateBeauty(interest, beautyOfSwiss);
		}
		
		int counter = 0;
		
		Random rnd = new Random();
		float threshold = 1;
		while(queue.size()>numberOfElements){
			recursiveJourney candidate = queue.poll();
			float result = candidate.computeDiversityRating()*0+candidate.beautyValue*10000;
			//float result = 1;
			threshold = threshold*0.99f+result*0.01f;
			if(result+100*rnd.nextFloat()-50>threshold){
				queue.add(candidate);
			}
			counter++;
			if(counter>1000){
				int a=1;
			}
			//System.out.println("removed elements in "+counter+"steps. Threshold: "+ threshold+" result: "+result);
		}
		//System.out.println("removed elements in "+counter+"steps");
		
			while(queue.size()>numberOfElements){
			queue.poll();}
	}

	
	static void decreaseLoadV2(LinkedList<recursiveJourney> queue, int numberOfElements, beautyOfSwitzerland beautyOfSwiss, HashMap<Integer, Boolean> interest, List<recursiveJourney>mindList){

		for(recursiveJourney journey:queue){
			journey.RateBeauty(interest, beautyOfSwiss);
			journey.originality = 0;
			for(recursiveJourney mJourney: mindList){
				journey.compareSimilarity(mJourney);
			}
		}
		
		
		Collections.sort(queue, new Comparator<recursiveJourney>() {
	        public int compare(recursiveJourney o1, recursiveJourney o2) {
	        	if(o1.beautyValue+o1.originality>o2.beautyValue+o2.originality)return 1;
	        	else if(o1.beautyValue+o1.originality==o2.beautyValue+o2.originality)return 0;
	        	else return -1;
	        }
	    });
		
		while(queue.size()>numberOfElements){
			queue.removeFirst();
		}
	}
	
	
	static class beautyOfSwitzerland{
		class beautyObject{
			String name;
			float xCoord;
			float yCoord;
			int ID;	
			char type;
		}
		public List<beautyObject> beList = new LinkedList<>();
		public beautyOfSwitzerland(BufferedReader reader){
			String nextLine = null;
			while(true){
				beautyObject bObject = new beautyObject();
				try {
					nextLine = reader.readLine();
					if(nextLine==null)break;
					bObject.name = nextLine;
					bObject.ID = Integer.parseInt(reader.readLine());
					bObject.xCoord = Float.parseFloat(reader.readLine());
					bObject.yCoord = Float.parseFloat(reader.readLine());
					bObject.type = reader.readLine().charAt(0);
				} catch (IOException e) {
					e.printStackTrace();
				}
				beList.add(bObject);
			}
		}
		
	}
	
	
	static int LevenshteinDistance(String one, String two){
		int[][] d = new int[one.length()+1][two.length()+1];
		for(int i = 0; i<=one.length(); i++)d[i][0]=i;
		for(int j = 0; j<=two.length(); j++)d[0][j]=j;
		
		for(int i = 1; i<=one.length(); i++){
			for(int j = 1; j<=two.length(); j++){
				if(one.charAt(i-1)==two.charAt(j-1))d[i][j]=d[i-1][j-1];
				else
				{
					d[i][j]=Math.min(d[i-1][j]+1, Math.min(d[i][j-1]+1, d[i-1][j-1]+1));
				}
			}
		}
		return d[one.length()][two.length()];
	}

}
