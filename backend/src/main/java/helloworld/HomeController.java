package helloworld;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

	SBB_data data;
	SBB_Magic.beautyOfSwitzerland beauty;
	
	// Constructor
	public HomeController(){
	        init();
	}
	
	public void init(){
		data = new SBB_data();
        String serviceFile = "FPLAN";
        String stationFile = "BFKOORD";
        BufferedReader br;
        System.out.println("read services");
		try {
			br = new BufferedReader(new FileReader(new File("res/" + serviceFile)));
	        data.addServices(br);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        System.out.println("read stations");
		try {
			br = new BufferedReader(new FileReader(new File("res/" + stationFile)));
	        data.addStations(br);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        System.out.println("connectData");
		data.connect();
		
        beauty = null;
        try {
			br = new BufferedReader(new FileReader(new File("res/POI")));
			beauty = new SBB_Magic.beautyOfSwitzerland(br);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	
    @Autowired ApplicationInstanceInfo instanceInfo;

    @RequestMapping("/")
    @ResponseBody
    public String test() {        
        return "Hello World. Welcome to Journey 3.2.";
    }
    
    @RequestMapping("/API")
    @ResponseBody
    public String getJourney(@RequestParam("from") String string_station1, @RequestParam("to") String string_station2,
    							@RequestParam("departure") String string_time1, @RequestParam("arrival") String string_time2,
    							@RequestParam("places") String places){
    	
    	String[] placeArray = new String[0];
    	
    	if (places != null && !places.equals("")){
    		places.split(",");
    	}
    	
    	HashMap<Integer, Boolean> map = new HashMap<Integer, Boolean>();
    	for (String place : placeArray){
    		if (place != null && !place.equals("")){
	    		int placenumber = Integer.parseInt(place);
	    		map.put(placenumber, true);
	    		System.out.println("place Number activated: " + placenumber);
    		}
    	}
    	
    	if (data == null || beauty == null){
    		init();
    		System.out.println("Loading data");
    	}
    	
    	SBB_data.Train_station station1 = data.smartSearch(string_station1);
        SBB_data.Train_station station2 = data.smartSearch(string_station2);
        
        SBB_data.SBB_time time1 = new SBB_data.SBB_time(string_time1);
        SBB_data.SBB_time time2 = new SBB_data.SBB_time(string_time2);
        
        System.out.println("Station " + string_station1 + " to " + string_station2);
        System.out.println("Time " + string_time1 + " to " + string_time2);
        
        List<SBB_Magic.recursiveJourney> journeys = SBB_Magic.getDiverseRoute(time1,
        		station1,
        		time2,
        		station2,
        		beauty,
        		map,
        		5);
        
         return SBB_Magic.getJSON(journeys);
    }
    

}