angular.module('starter.services', [])

  .factory('RouteDetails', function() {
    var routeDetails = {
      from: "",
      to: "",
      departureTimeStart: "Earliest arrival time",
      departureTimeEnd: "Latest arrival time"
    };
    return {
      getRouteDetails: function() {
        return routeDetails;
      }
    }
  })

  .factory('JourneyDetails', function() {
    var journeyDetails = {
      from: "",
      to: "",
      departureTimeStart: "Earliest arrival time",
      departureTimeEnd: "Latest arrival time"
    };
    return {
      getJourneyDetails: function() {
        return journeyDetails;
      }
    }
  })

  .factory('Settings', function() {

    var settingsOptions = {
      subscriptions: [
        {id: 1, name: "GA"},
        {id: 2, name: "Halbtax"},
        {id: 3, name: "Gleis 7"}
      ]
    };

    var settings = {
      isOffline: false,
      subscriptions: [],
      favorites: [
        {
          name: "Zurich"
        },
        {
          name: "Bern"
        }
      ],
      frequentlyUsed: [
        {
          name: "Luzern",
          counter: 0,
          isDeleted: false
        }
      ]
    };

    return {
      getSettings: function() {
        return settings;
      },
      getSettingsOptions: function() {
        return settingsOptions;
      },
      incrementStationCounter: function(station) {
        var found = false;
        settings.frequentlyUsed.forEach(function(item) {
          if (item.name === station) {
            item.counter++;
            found = true;
          }
        });
        if (!found) {
          settings.frequentlyUsed.push({
            name: station,
            counter: 1,
            isDeleted: false
          });
        }
      }
    };
  })

  .factory('TimeConverter', function(){

    return {
      convert: function(seconds){
        var hours = parseInt( seconds / 3600 ) % 24;
        var min = parseInt( seconds / 60 ) % 60;

        if (hours < 10 && hours >= 0){
          hours = "0" + hours;
        }

        if (min < 10 && min >= 0){
          min = "0" + min;
        }

        return hours + ":" + min;
      }
    }
  })

  .factory('JourneyRequester', function($http) {

    var formatTime = function(time) {
      console.log("formatting time");
      var temp = time.split(':');
      console.log("formatting time2");

      var newTime = temp.join('');
      console.log("formatting time3");

      while(newTime.length < 5) {
        newTime = "0" + newTime;
      }
      console.log("formatting time4");

      return newTime;
    };

    var makeUrl = function(from, to, startTime, endTime, places) {
      console.log("making url");
      var temp = [];

      places.forEach(function(item) {
        if (item.isActive) {
          temp.push(item.id);
        }
      });

      var placesString = temp.join();

      return "https://journey-cloud.scapp.io/API?from=" + from + "&to=" + to + "&departure=" + formatTime(startTime) + "&arrival=" + formatTime(endTime) + "&places="+placesString;
    };

    return {
      getConnections: function (from, to, startTime, endTime, places, callback) {
        var url = makeUrl(from, to, startTime, endTime, places);
        console.log(url);
        $http.get(url, {timeout: 4000}).
        success(function (data, status, headers, config) {
          // this callback will be called asynchronously
          // when the response is available
          callback(false, data);
        }).
        error(function (data, status, headers, config) {
          callback(true);
        });
        /*
         {"connections":[{"sections":[{"type:":"S ","departure":"17:22","arrival":"17:29","departurestation":"Z�rich HB","arrivalstation":"Z�rich Oerlikon","attractions":[{"ID":"1","name":"Zürich","type":"c"}]},{"type:":"S ","departure":"17:30","arrival":"17:37","departurestation":"Z�rich Oerlikon","arrivalstation":"Z�rich HB","attractions":[]},{"type:":"IR ","departure":"18:06","arrival":"18:21","departurestation":"Z�rich HB","arrivalstation":"Baden","attractions":[]}]},{"sections":[{"type:":"IR ","departure":"17:09","arrival":"17:57","departurestation":"Z�rich HB","arrivalstation":"Frick","attractions":[{"ID":"11","name":"Üetliberg","type":"m"}]},{"type:":"IR ","departure":"18:03","arrival":"18:27","departurestation":"Frick","arrivalstation":"Baden","attractions":[]}]},{"sections":[{"type:":"S ","departure":"14:10","arrival":"14:46","departurestation":"Z�rich HB","arrivalstation":"Rapperswil","attractions":[]},{"type:":"S ","departure":"17:10","arrival":"17:47","departurestation":"Rapperswil","arrivalstation":"Z�rich HB","attractions":[{"ID":"4","name":"Zürichsee","type":"l"}]},{"type:":"IR ","departure":"18:06","arrival":"18:21","departurestation":"Z�rich HB","arrivalstation":"Baden","attractions":[]}]}]}
         */
      }
    }
  })

  .factory('JourneyHistory', function() {
    var journeys = [];
    return {
      getLatest: function() {
        return journeys.length > 0 ? journeys[journeys.length-1] : {};
      },
      addJourney: function(journey) {
        journeys.push(journey);
      },
      getSpecificJourney: function(key) {
        return journeys[journeys.length-1].connections.length > key ? journeys[journeys.length-1].connections[key] : {};
      }
    }
  })

  .factory('RouteHistory', function() {
    var routes = [];
    return {
      getLatest: function() {
        return routes.length > 0 ? routes[routes.length-1] : {};
      },
      addRoute: function(route) {
        routes.push(route);
      },
      getSpecificRoute: function(key) {
        return routes[routes.length-1].connections.length > key ? routes[routes.length-1].connections[key] : {};
      }
    }
  })

  .factory('RouteRequest', function($http) {

    return {
      getConnections: function(from, to, time, callback) {
        var url = 'http://transport.opendata.ch/v1/connections?from='+from+'&to='+to+'&isArrivalTime=1&time='+time;
        console.log(url);
        $http.get(url, {timeout: 4000}).
        success(function(data, status, headers, config) {
          // this callback will be called asynchronously
          // when the response is available
          callback(false, data);
        }).
        error(function(data, status, headers, config) {
          callback(true);
        });
         /*
        callback(false, {"connections":[{"from":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T01:00:00+0200","departureTimestamp":1443826800,"delay":null,"platform":"41\/42","prognosis":{"platform":"41\/42","arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"to":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T01:27:00+0200","arrivalTimestamp":1443828420,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":"3","arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}},"duration":"00d00:27:00","transfers":0,"service":{"regular":"Sa, Su","irregular":null},"products":["SN"],"capacity1st":1,"capacity2nd":3,"sections":[{"journey":{"name":"SN 1 13710","category":"SN","categoryCode":5,"number":"13710","operator":"SBB","to":"Aarau","passList":[{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T01:00:00+0200","departureTimestamp":1443826800,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},{"station":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null},"arrival":"2015-10-03T01:02:00+0200","arrivalTimestamp":1443826920,"departure":"2015-10-03T01:02:00+0200","departureTimestamp":1443826920,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null}},{"station":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null},"arrival":"2015-10-03T01:05:00+0200","arrivalTimestamp":1443827100,"departure":"2015-10-03T01:05:00+0200","departureTimestamp":1443827100,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null}},{"station":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null},"arrival":"2015-10-03T01:07:00+0200","arrivalTimestamp":1443827220,"departure":"2015-10-03T01:07:00+0200","departureTimestamp":1443827220,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null}},{"station":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null},"arrival":"2015-10-03T01:10:00+0200","arrivalTimestamp":1443827400,"departure":"2015-10-03T01:10:00+0200","departureTimestamp":1443827400,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null}},{"station":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null},"arrival":"2015-10-03T01:13:00+0200","arrivalTimestamp":1443827580,"departure":"2015-10-03T01:15:00+0200","departureTimestamp":1443827700,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null}},{"station":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null},"arrival":"2015-10-03T01:19:00+0200","arrivalTimestamp":1443827940,"departure":"2015-10-03T01:19:00+0200","departureTimestamp":1443827940,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null}},{"station":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null},"arrival":"2015-10-03T01:21:00+0200","arrivalTimestamp":1443828060,"departure":"2015-10-03T01:21:00+0200","departureTimestamp":1443828060,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null}},{"station":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null},"arrival":"2015-10-03T01:24:00+0200","arrivalTimestamp":1443828240,"departure":"2015-10-03T01:24:00+0200","departureTimestamp":1443828240,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null}},{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T01:27:00+0200","arrivalTimestamp":1443828420,"departure":"2015-10-03T01:28:00+0200","departureTimestamp":1443828480,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}],"capacity1st":1,"capacity2nd":3},"walk":null,"departure":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T01:00:00+0200","departureTimestamp":1443826800,"delay":null,"platform":"41\/42","prognosis":{"platform":"41\/42","arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"arrival":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T01:27:00+0200","arrivalTimestamp":1443828420,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":"3","arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}}]},{"from":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T02:00:00+0200","departureTimestamp":1443830400,"delay":null,"platform":"41\/42","prognosis":{"platform":"41\/42","arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"to":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T02:27:00+0200","arrivalTimestamp":1443832020,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":"3","arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}},"duration":"00d00:27:00","transfers":0,"service":{"regular":"Sa, Su","irregular":null},"products":["SN"],"capacity1st":1,"capacity2nd":3,"sections":[{"journey":{"name":"SN 1 13712","category":"SN","categoryCode":5,"number":"13712","operator":"SBB","to":"Aarau","passList":[{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":"2015-10-03T01:58:00+0200","arrivalTimestamp":1443830280,"departure":"2015-10-03T02:00:00+0200","departureTimestamp":1443830400,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},{"station":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null},"arrival":"2015-10-03T02:02:00+0200","arrivalTimestamp":1443830520,"departure":"2015-10-03T02:02:00+0200","departureTimestamp":1443830520,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null}},{"station":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null},"arrival":"2015-10-03T02:05:00+0200","arrivalTimestamp":1443830700,"departure":"2015-10-03T02:05:00+0200","departureTimestamp":1443830700,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null}},{"station":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null},"arrival":"2015-10-03T02:07:00+0200","arrivalTimestamp":1443830820,"departure":"2015-10-03T02:07:00+0200","departureTimestamp":1443830820,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null}},{"station":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null},"arrival":"2015-10-03T02:10:00+0200","arrivalTimestamp":1443831000,"departure":"2015-10-03T02:10:00+0200","departureTimestamp":1443831000,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null}},{"station":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null},"arrival":"2015-10-03T02:13:00+0200","arrivalTimestamp":1443831180,"departure":"2015-10-03T02:15:00+0200","departureTimestamp":1443831300,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null}},{"station":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null},"arrival":"2015-10-03T02:19:00+0200","arrivalTimestamp":1443831540,"departure":"2015-10-03T02:19:00+0200","departureTimestamp":1443831540,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null}},{"station":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null},"arrival":"2015-10-03T02:21:00+0200","arrivalTimestamp":1443831660,"departure":"2015-10-03T02:21:00+0200","departureTimestamp":1443831660,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null}},{"station":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null},"arrival":"2015-10-03T02:24:00+0200","arrivalTimestamp":1443831840,"departure":"2015-10-03T02:24:00+0200","departureTimestamp":1443831840,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null}},{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T02:27:00+0200","arrivalTimestamp":1443832020,"departure":"2015-10-03T02:28:00+0200","departureTimestamp":1443832080,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}],"capacity1st":1,"capacity2nd":3},"walk":null,"departure":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T02:00:00+0200","departureTimestamp":1443830400,"delay":null,"platform":"41\/42","prognosis":{"platform":"41\/42","arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"arrival":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T02:27:00+0200","arrivalTimestamp":1443832020,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":"3","arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}}]},{"from":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T03:00:00+0200","departureTimestamp":1443834000,"delay":null,"platform":"41\/42","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"to":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T03:27:00+0200","arrivalTimestamp":1443835620,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}},"duration":"00d00:27:00","transfers":0,"service":{"regular":"Sa, Su","irregular":null},"products":["SN"],"capacity1st":1,"capacity2nd":3,"sections":[{"journey":{"name":"SN 1 13714","category":"SN","categoryCode":5,"number":"13714","operator":"SBB","to":"Aarau","passList":[{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":"2015-10-03T02:58:00+0200","arrivalTimestamp":1443833880,"departure":"2015-10-03T03:00:00+0200","departureTimestamp":1443834000,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},{"station":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null},"arrival":"2015-10-03T03:02:00+0200","arrivalTimestamp":1443834120,"departure":"2015-10-03T03:02:00+0200","departureTimestamp":1443834120,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":3},"location":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null}},{"station":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null},"arrival":"2015-10-03T03:05:00+0200","arrivalTimestamp":1443834300,"departure":"2015-10-03T03:05:00+0200","departureTimestamp":1443834300,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null}},{"station":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null},"arrival":"2015-10-03T03:07:00+0200","arrivalTimestamp":1443834420,"departure":"2015-10-03T03:07:00+0200","departureTimestamp":1443834420,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null}},{"station":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null},"arrival":"2015-10-03T03:10:00+0200","arrivalTimestamp":1443834600,"departure":"2015-10-03T03:10:00+0200","departureTimestamp":1443834600,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null}},{"station":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null},"arrival":"2015-10-03T03:13:00+0200","arrivalTimestamp":1443834780,"departure":"2015-10-03T03:15:00+0200","departureTimestamp":1443834900,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null}},{"station":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null},"arrival":"2015-10-03T03:19:00+0200","arrivalTimestamp":1443835140,"departure":"2015-10-03T03:19:00+0200","departureTimestamp":1443835140,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null}},{"station":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null},"arrival":"2015-10-03T03:21:00+0200","arrivalTimestamp":1443835260,"departure":"2015-10-03T03:21:00+0200","departureTimestamp":1443835260,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null}},{"station":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null},"arrival":"2015-10-03T03:24:00+0200","arrivalTimestamp":1443835440,"departure":"2015-10-03T03:24:00+0200","departureTimestamp":1443835440,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null}},{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T03:27:00+0200","arrivalTimestamp":1443835620,"departure":"2015-10-03T03:28:00+0200","departureTimestamp":1443835680,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}],"capacity1st":1,"capacity2nd":3},"walk":null,"departure":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T03:00:00+0200","departureTimestamp":1443834000,"delay":null,"platform":"41\/42","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"arrival":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T03:27:00+0200","arrivalTimestamp":1443835620,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}}]},{"from":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T04:00:00+0200","departureTimestamp":1443837600,"delay":null,"platform":"41\/42","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"to":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T04:27:00+0200","arrivalTimestamp":1443839220,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}},"duration":"00d00:27:00","transfers":0,"service":{"regular":"Sa, Su","irregular":null},"products":["SN"],"capacity1st":1,"capacity2nd":2,"sections":[{"journey":{"name":"SN 1 13716","category":"SN","categoryCode":5,"number":"13716","operator":"SBB","to":"Aarau","passList":[{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":"2015-10-03T03:58:00+0200","arrivalTimestamp":1443837480,"departure":"2015-10-03T04:00:00+0200","departureTimestamp":1443837600,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},{"station":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null},"arrival":"2015-10-03T04:02:00+0200","arrivalTimestamp":1443837720,"departure":"2015-10-03T04:02:00+0200","departureTimestamp":1443837720,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503020","name":"Z\u00fcrich Hardbr\u00fccke","score":null,"coordinate":{"type":"WGS84","x":47.385197,"y":8.517108},"distance":null}},{"station":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null},"arrival":"2015-10-03T04:05:00+0200","arrivalTimestamp":1443837900,"departure":"2015-10-03T04:05:00+0200","departureTimestamp":1443837900,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":2},"location":{"id":"008503001","name":"Z\u00fcrich Altstetten","score":null,"coordinate":{"type":"WGS84","x":47.391481,"y":8.488936},"distance":null}},{"station":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null},"arrival":"2015-10-03T04:07:00+0200","arrivalTimestamp":1443838020,"departure":"2015-10-03T04:07:00+0200","departureTimestamp":1443838020,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503509","name":"Schlieren","score":null,"coordinate":{"type":"WGS84","x":47.399175,"y":8.447226},"distance":null}},{"station":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null},"arrival":"2015-10-03T04:10:00+0200","arrivalTimestamp":1443838200,"departure":"2015-10-03T04:10:00+0200","departureTimestamp":1443838200,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503512","name":"Glanzenberg","score":null,"coordinate":{"type":"WGS84","x":47.398879,"y":8.42042},"distance":null}},{"station":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null},"arrival":"2015-10-03T04:13:00+0200","arrivalTimestamp":1443838380,"departure":"2015-10-03T04:15:00+0200","departureTimestamp":1443838500,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503508","name":"Dietikon","score":null,"coordinate":{"type":"WGS84","x":47.405774,"y":8.404968},"distance":null}},{"station":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null},"arrival":"2015-10-03T04:19:00+0200","arrivalTimestamp":1443838740,"departure":"2015-10-03T04:19:00+0200","departureTimestamp":1443838740,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503506","name":"Killwangen-Spreitenbach","score":null,"coordinate":{"type":"WGS84","x":47.433326,"y":8.354196},"distance":null}},{"station":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null},"arrival":"2015-10-03T04:21:00+0200","arrivalTimestamp":1443838860,"departure":"2015-10-03T04:21:00+0200","departureTimestamp":1443838860,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503511","name":"Neuenhof","score":null,"coordinate":{"type":"WGS84","x":47.450657,"y":8.331229},"distance":null}},{"station":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null},"arrival":"2015-10-03T04:24:00+0200","arrivalTimestamp":1443839040,"departure":"2015-10-03T04:24:00+0200","departureTimestamp":1443839040,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503505","name":"Wettingen","score":null,"coordinate":{"type":"WGS84","x":47.459637,"y":8.315983},"distance":null}},{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T04:27:00+0200","arrivalTimestamp":1443839220,"departure":"2015-10-03T04:28:00+0200","departureTimestamp":1443839280,"delay":null,"platform":"","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}],"capacity1st":1,"capacity2nd":2},"walk":null,"departure":{"station":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"arrival":null,"arrivalTimestamp":null,"departure":"2015-10-03T04:00:00+0200","departureTimestamp":1443837600,"delay":null,"platform":"41\/42","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":1,"capacity2nd":1},"location":{"id":"008503000","name":"Z\u00fcrich HB","score":null,"coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}},"arrival":{"station":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"arrival":"2015-10-03T04:27:00+0200","arrivalTimestamp":1443839220,"departure":null,"departureTimestamp":null,"delay":null,"platform":"3","prognosis":{"platform":null,"arrival":null,"departure":null,"capacity1st":null,"capacity2nd":null},"location":{"id":"008503504","name":"Baden","score":null,"coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}}}]}],"from":{"id":"008503000","name":"Z\u00fcrich HB","score":"101","coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null},"to":{"id":"008503504","name":"Baden","score":"101","coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null},"stations":{"from":[{"id":"008503000","name":"Z\u00fcrich HB","score":"101","coordinate":{"type":"WGS84","x":47.378177,"y":8.540192},"distance":null}],"to":[{"id":"008503504","name":"Baden","score":"101","coordinate":{"type":"WGS84","x":47.47642,"y":8.307695},"distance":null}]}});*/
      }
    };
  });
