angular.module('starter.controllers', [])
.controller('FavCtrl', function($scope, Settings) {

  $scope.status = "nothing";

  $scope.click = function() {
    $scope.status = "clicked";
  };
  $scope.drag = function() {
    $scope.status = "drag";
  };
  $scope.release = function(data) {
    $scope.endStation = data;
    $scope.status = "release";
  };

  $scope.settings = Settings.getSettings();
})

  .controller('FavSettingsCtrl', function($scope, $ionicModal, Settings) {
    $scope.listCanSwipe = true;
    $scope.settings = Settings.getSettings();

    $ionicModal.fromTemplateUrl('templates/addFavorite.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.closeAddFavorite = function() {
      $scope.modal.hide();
    };

    $scope.showAddFavorite = function() {
      $scope.modal.show();
    };

    $scope.addFavorite = function(station) {
      if (station) {
        $scope.settings.favorites.push({name: station});
        $scope.closeAddFavorite();
      }
    };

    $scope.deleteFrequentlyUsed = function(favorite) {
      $scope.settings.frequentlyUsed.forEach(function(item) {
        if (item.name === favorite.name) {
          item.isDeleted = true;
        }
      });
    }
  })

  .controller('DashCtrl', function($scope, $state, $ionicPopup, $ionicLoading, RouteDetails, RouteRequest, RouteHistory, TimeConverter, Settings) {

  $scope.routeDetails = RouteDetails.getRouteDetails();
  $scope.activeButton = 0;

  $scope.showAlert = function() {
    $ionicPopup.alert({
      title: 'Error',
      template: 'Please enter your departure and arrival station and time',
      buttons: [{
        text: 'OK',
        type: 'button-positive'
      }]
    });
  };

  // Time Picker Settings
  $scope.slots = {epochTime: 45000, format: 24, step: 10};

  // Time Picker Callback
  $scope.timePickerCallback = function (val) {
    if (typeof (val) === 'undefined') {
      console.log('Time not selected');
    } else {
      $scope.routeDetails.departureTimeStart = TimeConverter.convert(val);  // `val` will contain the selected time in epoch
    }
  };

  $scope.timePickerCallbackEnd = function (val) {
    if (typeof (val) === 'undefined') {
      console.log('Time not selected');
    } else {
      $scope.routeDetails.departureTimeEnd = TimeConverter.convert(val);  // `val` will contain the selected time in epoch
    }
  };

  $scope.search = function() {
    if ($scope.routeDetails.from && $scope.routeDetails.to && $scope.routeDetails.departureTimeStart.length < 6 && $scope.routeDetails.departureTimeEnd.length < 6) {
      $ionicLoading.show({
        template: 'Loading...'
      });

      RouteRequest.getConnections($scope.routeDetails.from, $scope.routeDetails.to, $scope.routeDetails.departureTimeStart, function(err, data) {
        RouteHistory.addRoute(data);
        console.log(data);
        Settings.incrementStationCounter($scope.routeDetails.from);
        Settings.incrementStationCounter($scope.routeDetails.to);
        $ionicLoading.hide();
        $state.go('tab.dash-results');
      });

    } else {
      $scope.showAlert();
    }
  }
})

  .controller('ScheduleListCtrl', function($scope, RouteDetails, RouteHistory) {
    $scope.routeDetails = RouteDetails.getRouteDetails();
    $scope.route = RouteHistory.getLatest();
  })

  .controller('ScheduleDetailCtrl', function($scope, $stateParams, $ionicScrollDelegate, RouteDetails, RouteHistory) {
    $scope.routeDetails = RouteDetails.getRouteDetails();
    console.log($stateParams.routeId);
    $scope.route = RouteHistory.getSpecificRoute($stateParams.routeId);

    $scope.onContentScroll = function(){
      var scrollDelegate = $ionicScrollDelegate.$getByHandle('contentScroll');
      var scrollView = scrollDelegate.getScrollView();
      $scope.$broadcast('contentScroll.scroll', scrollView);
    };
  })

  .controller('JourneyCtrl', function($scope, $state, $ionicPopup, $ionicLoading, JourneyDetails, JourneyRequester, JourneyHistory, TimeConverter, Settings) {

  $scope.journeyDetails = JourneyDetails.getJourneyDetails();

    // Time Picker Settings
    $scope.slots = {epochTime: 45000, format: 24, step: 10};

    // Time Picker Callback
    $scope.timePickerCallback = function (val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        $scope.journeyDetails.departureTimeStart = TimeConverter.convert(val);  // `val` will contain the selected time in epoch
      }
    };

    $scope.timePickerCallbackEnd = function (val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        $scope.journeyDetails.departureTimeEnd = TimeConverter.convert(val);  // `val` will contain the selected time in epoch
      }
    };

    $scope.places = [
      {
        id: 0,
        name: "Matterhorn",
        source: "Matterhorn.jpg",
        isActive: 0
      },
      {
        id: 1,
        name: "Eiger",
        source: "Eiger.jpg",
        isActive: 0
      },
      {
        id: 2,
        name: "Gotthard",
        source: "Gotthard.jpg",
        isActive: 0
      },
      {
        id: 3,
        name: "Üetliberg",
        source: "Uetliberg.jpg",
        isActive: 0
      },
      {
        id: 4,
        name: "Lac Leman",
        source: "Lac_Leman.jpg",
        isActive: 0
      },
      {
        id: 5,
        name: "Lake Lucerne",
        source: "Vierwaldstaettersee.jpg",
        isActive: 0
      },
      {
        id: 6,
        name: "Lake Lugano",
        source: "Luganersee.jpg",
        isActive: 0
      },
      {
        id: 7,
        name: "Lake Zurich",
        source: "Zuerichsee.jpg",
        isActive: 0
      },
      {
        id: 8,
        name: "Bern",
        source: "Bern.jpg",
        isActive: 0
      },
      {
        id: 9,
        name: "Lausanne",
        source: "Lausanne.jpg",
        isActive: 0
      },
      {
        id: 10,
        name: "Zurich",
        source: "Zurich.jpg",
        isActive: 0
      },
      {
        id: 11,
        name: "Basel",
        source: "Basel.jpg",
        isActive: 0
      }
    ];

    $scope.journeyDetails = JourneyDetails.getJourneyDetails();

    $scope.showAlert = function() {
      $ionicPopup.alert({
        title: 'Error',
        template: 'Please enter your departure and arrival station and time',
        buttons: [{
          text: 'OK',
          type: 'button-positive'
        }]
      });
    };


    $scope.search = function() {
      if ($scope.journeyDetails.from && $scope.journeyDetails.to && $scope.journeyDetails.departureTimeStart.length < 6 && $scope.journeyDetails.departureTimeEnd.length < 6) {
        $ionicLoading.show({
          template: 'Loading...'
        });

        JourneyRequester.getConnections($scope.journeyDetails.from, $scope.journeyDetails.to, $scope.journeyDetails.departureTimeStart, $scope.journeyDetails.departureTimeEnd, $scope.places, function(err, data) {
          JourneyHistory.addJourney(data);
          //console.log(data);
          Settings.incrementStationCounter($scope.journeyDetails.from);
          Settings.incrementStationCounter($scope.journeyDetails.to);
          $ionicLoading.hide();
          $state.go('tab.journey-results');
        });

      } else {
        $scope.showAlert();
      }
    };

    $scope.toggle = function(index){
      $scope.places[index].isActive = !$scope.places[index].isActive;
    }

  })

  .controller('JourneyListCtrl', function($scope, JourneyDetails, JourneyHistory) {
    $scope.routeDetails = JourneyDetails.getJourneyDetails();
    $scope.route = JourneyHistory.getLatest();
  })

  .controller('JourneyDetailCtrl', function($scope, $stateParams, $ionicScrollDelegate, JourneyDetails, JourneyHistory) {
    $scope.routeDetails = JourneyDetails.getJourneyDetails();
    console.log($stateParams.journeyId);
    $scope.route = JourneyHistory.getSpecificJourney($stateParams.journeyId);

    $scope.onContentScroll = function(){
      var scrollDelegate = $ionicScrollDelegate.$getByHandle('contentScroll');
      var scrollView = scrollDelegate.getScrollView();
      $scope.$broadcast('contentScroll.scroll', scrollView);
    };
  })

  .controller('SettingsCtrl', function($scope, Settings) {
    $scope.settings = Settings.getSettings();
    $scope.settingsOptions = Settings.getSettingsOptions();
})

  .directive('headerShrink', function($document) {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        var resizeFactor, scrollFactor, blurFactor;
        var header = $document[0].body.querySelector('.map');

        $scope.$on('contentScroll.scroll', function(event,scrollView) {
          if (scrollView.__scrollTop >= 0) {
            scrollFactor = scrollView.__scrollTop/2;
            header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + scrollFactor + 'px, 0)';
          } else {
            // shrink(header, $element[0], 0, headerHeight);
            resizeFactor = -scrollView.__scrollTop/100 + 0.99;
            //blurFactor = -scrollView.__scrollTop/100;
            header.style[ionic.CSS.TRANSFORM] = 'scale('+resizeFactor+','+resizeFactor+')';
            header.style.webkitFilter = 'blur('+blurFactor+'px)';
          }
        });
      }
    }
  })
/*
.directive('headerShrink', function($document) {
  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
      var resizeFactor, scrollFactor, blurFactor;
      var header = $document[0].body.querySelector('.map');

      $scope.$on('contentScroll.scroll', function(event,scrollView) {
        if (scrollView.__scrollTop >= 0) {
          scrollFactor = scrollView.__scrollTop/2;
          header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, +' + scrollFactor + 'px, 0)';
        } else {
          // shrink(header, $element[0], 0, headerHeight);
          resizeFactor = -scrollView.__scrollTop/100 + 0.99;
          blurFactor = -scrollView.__scrollTop/100;
          //if (blurFactor > 1.1) blurFactor = 1.1;
          header.style[ionic.CSS.TRANSFORM] = 'scale('+resizeFactor+','+resizeFactor+')';
          header.style.webkitFilter = 'blur('+blurFactor+'px)';
        }
      });
    }
  }
})*/
